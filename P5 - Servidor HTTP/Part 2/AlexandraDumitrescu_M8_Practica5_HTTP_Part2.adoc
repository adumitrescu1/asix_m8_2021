Alexandra Dumitrescu

= [underline]#*Pràctica 5. Servidor HTTP - Part 2*#
_M8 - Serveis de Xarxa i Internet_ 

=== *Apartat 4: Configuració del segon host virtual*
***

Editarem ara *dumitrescu.alexandra.lan.conf*.

El que primer farem serà afegir-li una *capa SSL*: 

  sudo touch dumitrescu-ssl.conf
  sudo cp default-ssl.conf dumitrescu-ssl.conf
  sudo nano dumitrescu-ssl.conf

La instrucció utilitzada per crear el *certificat autosignat* és: 

  sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt


==== *Example 1. Entregar*
***

* *Canvis fets en la configuració.*

image::imatgespractica5/1.png[]

==== *Example 2. Entregar*
***

Un cop modificada la configuració, activa el nou site.

* *Instrucció utilitzada.*

  sudo a2ensite dumitrescu-ssl.conf
  sudo a2enmod ssl

image::imatgespractica5/3.png[] 

==== *Example 3. Entregar*
***

* *Comprovació que funciona.*

image::imatgespractica5/4.png[]


==== *Example 4. Entregar*
***

* Forçarem ara que qualsevol petició dirigida a HTTP es transformi
*automàticament a HTTPS*. 

Primer modificarem *dumitrescu.alexandra.lan.conf* per habilitar .htaccess:

image::imatgespractica5/6.png[]

Després, editarem el *fitxer .htaccess*:

image::imatgespractica5/5.png[]
  
  sudo a2enmod 

* *Captura del fitxer de registre d’accés de l’Apache, on es veu la reescriptura
de l’adreça.* 

  sudo nano dumitrescu.alexandra.conf
  -> LogLevel debug rewrite:trace3

image::imatgespractica5/11.png[]
  
  sudo tail -f /var/log/apache2/error.log

image::imatgespractica5/10.png[]

=== *Apartat 5: Instal·lació de PHP*
***

Ara que tenim els hosts virtuals funcionant, procedirem a instal·lar els mòduls necessaris per a què el nostre servidor suporti projectes realitzats en PHP.

* Per a *instal·lar PHP* i que aquest es vinculi amb l’Apache prèviament instal·lat haurem d’executar la següent comanda:

  sudo apt install php libapache2-mod-php

image::imatgespractica5/7.png[]

==== *Example 5. Entregar*
***

* *Captura del resultat de la comanda php -v.*

image::imatgespractica5/8.png[]

Una vegada hem revisat que els paquets s’han instal·lat correctament, crearem un *petit script* per veure si Apache reconeix el mòdul recentment instal·lat.

Crea al primer host virtual un fitxer anomenat *info.php* amb el següent contingut:

  <?php
    phpinfo();
  ?>

==== *Example 6. Entregar*
***

* *Captura del resultat de còrrer l’script.*

image::imatgespractica5/9.png[]
