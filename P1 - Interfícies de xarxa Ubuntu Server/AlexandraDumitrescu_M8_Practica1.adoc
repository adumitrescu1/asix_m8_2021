
Alexandra Dumitrescu

= [navy - underline]#*Pràctica 1. Interfícies de xarxa en Ubuntu Server*#
[black]_M8 - Serveis de Xarxa i Internet_

[navy]*Aspectes sobre els quals tractarem:*

***

- [*] Format dels noms de les interfícies de xarxa.
- [*] Format del fitxer yaml utilitzat per la configuració de les interfícies via netplan.
- [*] Sintaxi del fitxer netplan. 
- [*] Comanda IP, usos, exemples més importants... 

***

Abans de començar, s'han de crear dos clons de la màquina base en la que utilitzem en Linux Server 20.04, aquests correspondran a Client1 i Client 2. 

[navy]*Com fer la clonació?*


====
. Seleccionem la màquina virtual del Servidor Linux que utilitzem com a *"Base"* i fem _clic_ en *"Clonar"* de les opcions que ens surt al menú. 
+
image:imatgespractica1/1.png[]

. Indiquem a VirtualBox el nom de la nostra nova màquina virtual i escollim que es *reinicialitzi l'adreça MAC* de la targeta de xarxa.  
+
image:imatgespractica1/2.png[]


. Finalment, caldrà arrancar la màquina i *canviar-li el nom* pel que li pertocaria. 


.. Executar *hostnamectl set-hostname <nou_nom_de_la_màquina>*.
+
image:imatgespractica1/3.png[]

.. Editar el fitxer */etc/hosts* i canviar el nom vell pel nou nom allà on
aparegui.
+
image:imatgespractica1/4.png[]


====


[navy]*1.1 Arxiu de configuració netplan*

Un cop creades les màquines virtuals el que farem serà cercar on està ubicat l'arxiu de configuració netplan.

====

L'arxiu de configuració netplan es troba situat en la carpeta */etc/* dins el directori */netplan/* en qüestió. El nom de l'arxiu, en el nostre cas, correspon a "*00-installer-config.yaml*".

image::imatgespractica1/5.png[]


====

[navy]*1.2 Configuració de netplan per funcionar amb DHCP*

Ja trobar l'arxiu de configuració netplan revisarem el seu contingut i contrastarem si està configurat per a funcionar amb DHCP. 

====

Efectivament, trobem que el servei DHCP està activat. 

image::imatgespractica1/6.png[]


====

[navy]*1.3 Canvi d'interfícies - Configuració IP estàtica netplan per comunicació entre clients*


Canviarem a Virtualbox les interfícies de les dues màquines a xarxa interna i configurarem amb Netplan les interfícies (amb IP estàtica) per a què puguin comunicar-se. (Rang d'IP 172.30.1.x per exemple).

====

*En el cas de Client 1*

Li canviarem la interfície a xarxa interna des de la configuració de la màquina virtual a l'apartat de *"Red"*. 

image::imatgespractica1/7.png[]

A més, també caldrà configurar la seva IP de dinàmica a estàtica. Per a fer-ho, el que haurem de fer serà tornar a buscar l'arxiu de configuració de netplan i modificar-lo. Després, per guardar els canvis realitzar s'haurà d'efectuar la comanda *sudo netplan apply*.

image::imatgespractica1/8.png[]

image::imatgespractica1/9.png[]

====

====

*En el cas de Client 2*

També li donarem una *interfície de xarxa interna*. El procediment és igual que per al Client 1 a l'hora de fer-ho. 

image::imatgespractica1/7.png[]

La seva IP estàtica correspondrà a *"172.30.1.3"*.

image::imatgespractica1/10.png[]

image::imatgespractica1/11.png[]

====

====

*Comprovació del seu funcionament*

Per veure que la configuració estàtica d'IP funciona correctament intentarem de fer un *ping des de Client 1 (172.30.1.2) a Client 2 (172.30.1.3)*. Si rebem resposta és que funciona correctament. 

image::imatgespractica1/12.png[]

A continuació, farem el mateix amb el Client 2 cap a Client 1. 

image::imatgespractica1/13.png[]

====

[navy]*1.4 Canvi d'interfície - Afegir en una de les màquines la interfície NAT a part de l'interna*

Tornarem a canviar a VirtualBox la interfície d'una de les dues màquines virtuals i crearem dues, una NAT i l'altra xarxa interna.

====

*Afegim NAT a Client 1* 

Un cop ja funcionant la xarxa interna, el que farem serà afegir-li una interfície NAT a part de la que ja disposa. Per fer-ho, anirem a la seva configuració, a l'apartat de *"Red"*, de manera que habilitarem el segon adaptador, el qual serà *"NAT"*.

image::imatgespractica1/14.png[]

====

====

*Configuració de l'arxiu netplan* 

En aquest cas, la màquina Client 1 ha de disposar d'una *configuració de xarxa NAT amb DHCP* i mantenir també la direcció IP estàtica de l'altra boca. La primera boca és mantindrà igual i afegirem la boca enp0s8 dinàmica a continuació corresponent a Internet. 

El fitxer ha de quedar d'aquesta manera:

image::imatgespractica1/15.png[]

====

====

*Comprovació del seu funcionament*

Ja configurat l'arxiu netplan podem verificar que Client 1 continua fent ping a Client 2.

image::imatgespractica1/16.png[]

També, ara ja té connectivitat a Internet. 

image::imatgespractica1/17.png[]

De manera que disposa de dos adaptadors de xarxa amb IP's diferents. 

image::imatgespractica1/18.png[]

====

***

[navy]*1.5 (Part Opcional) - IP estàtica + sortida Internet*

Per aquesta part és necessari tenir descarregades les OVA de *Thor* i *Heimdall*, les quals ja són operatives, i executar-les amb VirtualBox.

L'objectiu és configurar de nou els dos clients perquè amb la seva IP estàtica puguin *sortir a Internet* a través de les dues màquines importades. 

Perquè pugui funcionar, *totes les màquines hauran de trobar-se en la mateixa xarxa interna* (intnet3). 

====

*En el cas de Client 1* 

Se li haurà de *deshabilitar l'adaptador NAT* de xarxa per no tenir accés a internet a través d'una IP dinàmica. Ens haurem de preocupar també de canviar la seva IP estàtica per una dins del rang dels servidors. 

image::imatgespractica1/19.png[]

D'entrada, caldrà *esborrar la seva configuració DHCP* de l'arxiu netplan. Fent això, aprofitarem per modificar la seva IP per *"172.30.3.2"*. 

image::imatgespractica1/20.png[]

A més, li afegirem un *gateway* amb IP *"172.30.3.1"* que correspon a Thor, aquest a la vegada està ja connectat amb Heimdall, que disposa de sortida a Internet. També necessitarà *servidors DNS*, en aquest cas li donarem els de Google. 

image::imatgespractica1/21.png[]

Finalment, comprovarem la seva connectivitat a internet fent un ping a una pàgina web.

image::imatgespractica1/22.png[]


====

====

*En el cas de Client 2*

El procediment és igual que per al Client 1, de manera que l'*arxiu de configuració de xarxa netplan* ha de quedar de la manera següent: 

image::imatgespractica1/23.png[]

Comprovarem la seva connectivitat fent *ping a Google*.

image::imatgespractica1/24.png[]

