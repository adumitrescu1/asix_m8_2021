
Alexandra Dumitrescu

= [green - underline]#*Pràctica 2. Servidor DHCP*#
[black]_M8 - Serveis de Xarxa i Internet_

=== [green]*Preparatius*
***

* Crea un clon de base i canvia-li el nom a *Farnsworth*.

 sudo hostnamectl set-hostname Farnsworth

image::imatgespractica2/1.png[]

* A Farnsworth posa-li dues interfícies web, una configurada com a *NAT* i l’altra com a *xarxa interna (intnet)*.
 
* Configura la interfície NAT amb DHCP i la interna amb la IP *172.30.1.1*.

image::imatgespractica2/4.png[]


=== [green]*Apartat 1: Instal·lació del servidor DHCP a Farnsworth*
***

* Cerqueu el nom del paquet que conté el *servidor DHCP a Debian/Ubuntu* i instal·leu-lo.
 
 sudo apt install isc-dhcp-server
 
image::imatgespractica2/5.png[]

* El primer fitxer a modificar és /etc/default/isc-dhcp-server. Aquí cal modificar el paràmetre *INTERFACESv4* per indicar que s’han de servir IP dinàmiques a les peticions que arribin a la interfície de xarxa (interna) que té Farnsworth (la boca *enp0s8*).

 INTERFACESV4="enp0s8"

image::imatgespractica2/6.png[]

* El segon a modificar és el fitxer principal de configuració del servidor DHCP amb les opcions que volem. El seu nom és *dhcpd.conf* i es troba en /etc/dhcp. La instrucció que haurem d'executar per a què el servidor recarregui la nova configuració és *"sudo systemctl restart isc-dhcp-server.service"*. 

  /etc/dhcp/dhcpd.conf

image::imatgespractica2/7.png[]

  sudo systemctl restart isc-dhcp-server.service

image::imatgespractica2/8.png[]

* La captura del *registre* del sistema mostrant els missatges que llença el servidor DHCP. 

 sudo cat /var/log/syslog | grep dhcpd > registre.txt
 
En aquesta imatge es pot observar és que fa un repàs de la nostra configuració dhcp i com està actuant el servei. En el meu cas ara mateix no està funcionant ja que no hi tinc declarada ninguna subxarxa ni configuració per a les interfícies. 

image::imatgespractica2/9.png[]


=== [green]*Apartat 2: Servidor DHCP a la mateixa xarxa*
***

Volem configurar el *servidor DHCP* per tal que doni IP als ordinadors de la
xarxa 172.30.1.0/24. (Fer una *còpia de seguretat* abans de modificar l'arxiu)

 sudo cp dhcpd.conf dhcpd.conf.bak

image::imatgespractica2/10.png[]

* El servidor DHCP proporcionarà la següent *configuració* a totes les màquines
client, independentment del seu segment de xarxa:


 Servidor DNS primari: 172.30.1.1
 Servidor DNS secundari: 172.30.1.2
 Nom de domini: cognom1-cognom2.test. Per exemple en Pere Roca Grau tindrà un nom de domini roca-grau.test.
 Temps de concessió per defecte: una hora.
 Temps de concessió màxim: dues hores.

* Per a la xarxa *172.30.1.0/24*, configurarem les següents opcions:

 EL servei DHCP repartirà adreces IP en el rang 172.30.1.100 – 172.30.1.254 (ambdues incloses).
 La porta d’enllaç serà 172.30.1.1.
 
La *configuració DNS* ha de quedar de la següent manera: 

image::imatgespractica2/11.png[] 

Una vegada guardat l'arxiu, farem un *"restart" del servei DNS* per aplicar els canvis. 

 sudo systemctl restart isc-dhcp-server

image::imatgespractica2/12.png[]


=== [green]*Apartat 3: Configuració de la màquina client PC1 (a la mateixa xarxa)*
***

Per a dur a terme aquest apartat, necessitarem una altra màquina virtual de "base" anomenada *"PC1"*.

* Connectem temporalment la màquina PC1 a la xarxa *172.30.1.0/24*. Això ho fem
directament des del VirtualBox, connectant la seva targeta de xarxa al
switch *intnet*.

image::imatgespractica2/13.png[]

* Configurem la xarxa de la màquina PC1 de manera que *enp0s3 adquireixi IP
mitjançant DHCP*. Hauria d’estar així per defecte.

image::imatgespractica2/14.png[]

* A Farnsworth seguiu els canvis al fitxer de registre del sistema amb tail -f
/var/log/syslog.

 sudo tail -f /var/log/syslog 
 
image::imatgespractica2/15.png[] 

* Forceu la *renovació de l’adreça IP a PC1* amb dhclient.

 sudo dhclient

image::imatgespractica2/16.png[] 

* Observeu les noves línies que han aparegut al *syslog de Farnsworth*.

image::imatgespractica2/17.png[] 

* Observeu el tràfic generat a l’apartat anterior utilitzant el
*tcpdump a Farnsworth*. Utilitza les opcions -n i -v al tcpdump. -n evita que
s’intentin resoldre els noms (encara no tenim el DNS funcionant), i -v fa que
es mostri la informació completa del paquet capturat.

 sudo tcpdump -n -v -i enp0s8
 
image::imatgespractica2/18.png[] 

La comanda per *alliberar PC1 de la seva IP* es:

 sudo dhclient -r -v  
 
image::imatgespractica2/19.png[] 

Per *renovar la ip* utilitzarem:

 sudo dhclient -v 
 
image::imatgespractica2/20.png[] 

El procés que s'ha realitzat per a donar la nova IP a PC1 es pot veure gràcies al tcpdump: 

image::imatgespractica2/21.png[]  

***

Validació *adreça IP*:

image::imatgespractica2/22.png[] 

Validació *gateway*:

image::imatgespractica2/23.png[] 

Validació *servidors DNS*:

Per a que ens surti la informació relativa a quins servidors DNS tenim podem utilitzar la comanda:

 systemd-resolve --status
 
image::imatgespractica2/24.png[] 

***

* Forceu al client la *renovació de l’adreça*. Des del servidor, localitzeu
l’arxiu on s’emmagatzema el registre de les cessions DHCP, i trobeu el
fragment on s’ha registrat l’operació anterior. Ressalta la IP i l’hora de
cessió i l’hora de caducitat de la cessió.

*/var/lib/dhcp/dhcpd.leases*: conté informació actualitzada sobre les concessions que ha atorgat el servidor als clients. Dins d'aquest arxiu hi ha una entrada per cada concessió que s'ha donat i en la que s'indica la IP que s'ha donat al client, la seva direcció física, quant de temps s'ha utilitzat... 

image::imatgespractica2/25.png[]

En la captura anterior també es pot visualitzar la MAC de PC1 que correspon a *08:00:27:c1:b8:bd* en l'apartat de *hardware ethernet*. 
 